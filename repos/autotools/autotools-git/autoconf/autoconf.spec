%{?_compat_el5_build}

# Workaround to always have %scl defined (for testing locally)
%{!?scl:%global scl autotools-git}

%global commits_from_release    161
%global git_hash                ab30

%{?scl:%scl_package autoconf}

Summary:    A GNU tool for automatically configuring source code
Name:       %{?scl_prefix}autoconf
Version:    2.69
# Do not reset Release tag to '1' when rebasing!
Release:    14.%{commits_from_release}.%{git_hash}%{?dist}
Group:      Development/Tools
License:    GPLv2+ and GFDL
Source0:    autoconf-2.69.%{commits_from_release}-%{git_hash}-dirty.tar.gz
URL:        http://www.gnu.org/software/autoconf/
BuildArch: noarch


# run "make check" by default
%bcond_without check

# m4 >= 1.4.6 is required, >= 1.4.14 is recommended;  We have 1.4.5 in rhel5,
# 1.4.13 in rhel6, so don't build for el5 yet - this requires workaround with
# probably empty 'm4' SCLized package for OK platforms, don't know what is
# proper way.
BuildRequires:      %{?scl_prefix}m4, help2man
Requires:           %{?scl_prefix}m4
BuildRequires:      emacs

%{?_compat_br_perl_macros}

BuildRequires:      perl(Data::Dumper)
# from f19, Text::ParseWords is not the part of 'perl' package
BuildRequires:      perl(Text::ParseWords)
%{?scl:
BuildRequires: scl-utils-build
Requires:%scl_runtime
}
%if 0%{?fedora} >= 24 || 0%{?rhel} >= 8
BuildRequires: perl-generators
%endif

%if %{with check}
# For extended testsuite coverage
BuildRequires:      gcc-gfortran
%if 0%{?fedora} >= 15
BuildRequires:      erlang
%endif
%endif

Requires(post):     /sbin/install-info
Requires(preun):    /sbin/install-info

%if ! 0%{?rhel} == 5
# filter out bogus perl(Autom4te*) dependencies
%global __requires_exclude %{?__requires_exclude:%__requires_exclude|}^perl\\(Autom4te::
%global __provides_exclude %{?__provides_exclude:%__provides_exclude|}^perl\\(Autom4te::
%endif

%description
GNU's Autoconf is a tool for configuring source code and Makefiles.
Using Autoconf, programmers can create portable and configurable
packages, since the person building the package is allowed to
specify various configuration options.

You should install Autoconf if you are developing software and
would like to create shell scripts that configure your source code
packages. If you are installing Autoconf, you will also need to
install the GNU m4 package.

Note that the Autoconf package is not required for the end-user who
may be configuring software with an Autoconf-generated script;
Autoconf is only required for the generation of the scripts, not
their use.

%prep
%setup -q -n autoconf-%{version}.%{commits_from_release}-%{git_hash}-dirty


%build
%{?scl_enable}
%configure
# not parallel safe
make
%{?scl_disable}


%check
%if %{with check}
%{?scl_enable}
make check || : # TESTSUITEFLAGS='1-198 200-' # will disable nr. 199.
%{?scl_disable}
%endif


%install
%_compat_install

%if 0%{?rhel} == 5
rm -rf %{buildroot}
%endif
%{?scl_enable}
make install DESTDIR=%{buildroot}
mkdir -p %{buildroot}/share
%{?scl_disable}

# Don't %%exclude this in %%files as it is not generated on RHEL7
rm -rf %{buildroot}%{_infodir}/dir


%clean
%_compat_clean


%post
/sbin/install-info %{_infodir}/autoconf.info %{_infodir}/dir || :

%preun
if [ "$1" = 0 ]; then
    /sbin/install-info --del %{_infodir}/autoconf.info %{_infodir}/dir || :
fi


%files
%{_bindir}/*
%{_infodir}/autoconf.info*
# don't include standards.info, because it comes from binutils...
%exclude %{_infodir}/standards*
# don't include info's TOP directory
%{_datadir}/autoconf/
%dir %{_datadir}/emacs/
%{_datadir}/emacs/site-lisp/
%{_mandir}/man1/*
%doc AUTHORS COPYING* ChangeLog NEWS README THANKS TODO

%changelog
* Mon Oct 10 2016 Pavel Raiskup <praiskup@redhat.com> - 2.69-14.161.ab30
- BR perl-generators

* Mon Oct 10 2016 Pavel Raiskup <praiskup@redhat.com> - 2.69-13.161.ab30
- simplify and unify with autotools-latest

* Sun Oct 09 2016 Pavel Raiskup <praiskup@redhat.com> - 2.69-12.161.ab30
- bump: rebuild october 2016

* Fri Jun 17 2016 Pavel Raiskup <praiskup@redhat.com> - 2.69-10.161.ab30
- rebase for latest git version

* Wed Nov 04 2015 Pavel Raiskup <praiskup@redhat.com> - 2.69-10.147.5ad35
- rebase for latest git version

* Wed Aug 12 2015 Pavel Raiskup <praiskup@redhat.com> - 2.69-9.120.5dcda
- use _compat_el5_build only if defined (rhbz#1252751)

* Sun Jun 15 2014 Pavel Raiskup <praiskup@redhat.com> - 2.69-8.120.5dcda
- rebase for latest git version
- enable testsuite (do not fail, though)

* Sat May 24 2014 Pavel Raiskup <praiskup@redhat.com> - 2.69-7.118.4edc
- centralize hacks

* Fri May 23 2014 Pavel Raiskup <praiskup@redhat.com> - 2.69-6.118.4edc
- rebase for latest git version

* Mon Mar 24 2014 Pavel Raiskup <praiskup@redhat.com> - 2.69-5.117.1717
- remove buildroot in %%install

* Mon Mar 24 2014 Pavel Raiskup <praiskup@redhat.com> - 2.69-4.117.1717
- require the SCL-ized m4

* Fri Mar 21 2014 Pavel Raiskup <praiskup@redhat.com> - 2.69-3.117.1717
- build require %%scl_runtime properly

* Wed Mar 19 2014 Pavel Raiskup <praiskup@redhat.com> - 2.69-2.117.1717
- add help2man BR, fix BR for perl-macros

* Wed Mar 19 2014 Pavel Raiskup <praiskup@redhat.com> - 2.69-1.117.1717
- SCL-ized spec file from Rawhide, no testsuite enabled yet
