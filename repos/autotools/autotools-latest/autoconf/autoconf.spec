# Run extended test
%bcond_with   autoconf_enables_optional_test

%{?_compat_el5_build}

%{!?scl:%global scl autotools-latest}

%{?scl:%scl_package autoconf}

Summary:    A GNU tool for automatically configuring source code
Name:       %{?scl_prefix}autoconf
Version:    2.69
Release:    11%{?dist}
Group:      Development/Tools
License:    GPLv2+ and GFDL
Source0:    http://ftpmirror.gnu.org/autoconf/autoconf-%{version}.tar.gz
URL:        http://www.gnu.org/software/autoconf/

Patch1:     autoconf-2.69-perl-5.22-autoscan.patch

BuildArch:  noarch


# run "make check" by default
%bcond_without check

# m4 >= 1.4.6 is required, >= 1.4.14 is recommended;  We have 1.4.5 in rhel5,
# 1.4.13 in rhel6, so don't build for el5 yet - this requires workaround with
# probably empty 'm4' SCLized package for OK platforms, don't know what is
# proper way.
BuildRequires:      %{?scl_prefix}m4, help2man
Requires:           %{?scl_prefix}m4
BuildRequires:      gcc
BuildRequires:      emacs

%{?_compat_br_perl_macros}

BuildRequires:      perl(Data::Dumper)
# from f19, Text::ParseWords is not the part of 'perl' package
BuildRequires:      perl(Text::ParseWords)
%{?scl:
BuildRequires: scl-utils-build
Requires:%scl_runtime
}
%if 0%{?fedora} >= 24 || 0%{?rhel} >= 8
BuildRequires: perl-generators
%endif

%if %{with check}
%if %{with autoconf_enables_optional_test}
# For extended testsuite coverage
BuildRequires:      gcc-gfortran
%if 0%{?fedora} >= 15
BuildRequires:      erlang
%endif
%endif
%endif

Requires(post):     /sbin/install-info
Requires(preun):    /sbin/install-info

%if ! 0%{?rhel} == 5
# filter out bogus perl(Autom4te*) dependencies
%global __requires_exclude %{?__requires_exclude:%__requires_exclude|}^perl\\(Autom4te::
%global __provides_exclude %{?__provides_exclude:%__provides_exclude|}^perl\\(Autom4te::
%endif

%description
GNU's Autoconf is a tool for configuring source code and Makefiles.
Using Autoconf, programmers can create portable and configurable
packages, since the person building the package is allowed to
specify various configuration options.

You should install Autoconf if you are developing software and
would like to create shell scripts that configure your source code
packages. If you are installing Autoconf, you will also need to
install the GNU m4 package.

Note that the Autoconf package is not required for the end-user who
may be configuring software with an Autoconf-generated script;
Autoconf is only required for the generation of the scripts, not
their use.

%prep
%setup -q -n autoconf-%{version}
%patch1 -p1


%build
%{?scl_enable}
%configure
%make_build
%{?scl_disable}


%check
%if %{with check}
# make check # TESTSUITEFLAGS='1-198 200-' # will disable nr. 199.
# make check TESTSUITEFLAGS="-k \!erlang"
%{?scl_enable}
make check
%{?scl_disable}
%endif


%install
%{?_compat_install}
%{?scl_enable}
%make_install
%{?scl_disable}

# Don't %%exclude this in %%files as it is not generated on RHEL7
rm -rf %{buildroot}%{_infodir}/dir


%clean
%{?_compat_clean}


%post
/sbin/install-info %{_infodir}/autoconf.info %{_infodir}/dir || :

%preun
if [ "$1" = 0 ]; then
    /sbin/install-info --del %{_infodir}/autoconf.info %{_infodir}/dir || :
fi


%files
%{_bindir}/*
%{_infodir}/autoconf.info*
# don't include standards.info, because it comes from binutils...
%exclude %{_infodir}/standards*
# don't include info's TOP directory
%{_datadir}/autoconf/
%dir %{_datadir}/emacs/
%{_datadir}/emacs/site-lisp/
%{_mandir}/man1/*
%doc AUTHORS COPYING* ChangeLog NEWS README THANKS TODO

%changelog
* Fri Oct 12 2018 Pavel Raiskup <praiskup@redhat.com> - 2.69-11
- install gcc to fix testsuite

* Fri Oct 12 2018 Pavel Raiskup <praiskup@redhat.com> - 2.69-10
- cleanup, sync with rawhide

* Mon Oct 10 2016 Pavel Raiskup <praiskup@redhat.com> - 2.69-9
- BR perl-generators

* Mon Oct 10 2016 Pavel Raiskup <praiskup@redhat.com> - 2.69-8
- simplify and unify with autotools-git

* Sun Oct 09 2016 Pavel Raiskup <praiskup@redhat.com> - 2.69-7
- bump: rebuild september 2016

* Tue Sep 27 2016 Pavel Raiskup <praiskup@redhat.com> - 2.69-6
- bump, depend on epel-rpm-macros

* Fri Jun 17 2016 Pavel Raiskup <praiskup@redhat.com> - 2.69-5
- bump, autoconf got lost somewhere in automatic copr migration from 23 to 24

* Wed Aug 12 2015 Pavel Raiskup <praiskup@redhat.com> - 2.69-4
- use _compat_el5_build only if defined (rhbz#1252751)

* Thu May 29 2014 Pavel Raiskup <praiskup@redhat.com> - 2.69-3
- release bump for %%_compat_el5_build

* Tue Mar 25 2014 Pavel Raiskup <praiskup@redhat.com> - 2.69-2
- require the SCL-ized m4 so we may run on RHEL5

* Fri Mar 21 2014 Pavel Raiskup <praiskup@redhat.com> - 2.69-1
- copy SCL-ized spec from autotools-git repo
