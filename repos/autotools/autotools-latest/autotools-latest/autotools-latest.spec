%{?_compat_el5_build}

# Workaround to always have %%scl defined
%{!?scl:%global scl autotools-latest}

%{?scl_package:%scl_package %{scl}}

Summary: Package that installs %scl
Name: %scl
Version: 1
Release: 24%{?dist}
License: GPLv2+
Group: Applications/File

Requires: %{?scl_prefix}m4 %{?scl_prefix}automake
Requires: %{?scl_prefix}autoconf %{?scl_prefix}libtool
# Fix build even when scl-utils-build is not in minimal buildroot.
BuildRequires: scl-utils-build
# needed for %%_compat_scl_env_adjust
BuildRequires: compat-rpm-config


%description
This is the main package for %scl Software Collection.  It contains the latest
released (stable) versions of autotools.  Just run "scl enable %scl bash" to
make it work instead of system-default autotools.


%package runtime
Summary: Package that handles %scl Software Collection.
Group: Applications/File
Requires: scl-utils

%description runtime
Package shipping essential scripts to work with %scl Software Collection.


%package build
Summary: Package that handles %{scl} Software Collection.
Group: Applications/File
Requires: scl-utils-build
Requires: compat-rpm-config
Requires: scl-rpm-config

%description build
Package shipping essential configuration macros to build %{scl} Software
Collection or packages depending on %{scl} Software Collection.


%prep
%setup -c -T


%build
cat > enable <<\EOF
%_compat_scl_env_adjust PATH            %{_bindir}
%_compat_scl_env_adjust LIBRARY_PATH    %{_libdir}
%_compat_scl_env_adjust LD_LIBRARY_PATH %{_libdir}
%_compat_scl_env_adjust MANDIR          %{_mandir}
%_compat_scl_env_adjust INFOPATH        %{_infodir}
EOF


%install
mkdir -p %{buildroot}/%{_scl_scripts}/root
install -c -p -m 0644 enable %{buildroot}%{_scl_scripts}/enable
%scl_install

cat %{buildroot}/%{_root_sysconfdir}/rpm/macros.%{scl}-config
rm -rf %{buildroot}/%{_root_sysconfdir}/rpm/macros.%{scl}-config
%if 0%{?rhel} <= 6
mkdir -p %buildroot%_datadir/aclocal
%endif


%files


%files build


%files runtime
%scl_files
%if 0%{?rhel} == 7
# Temporary fix for some bug in scl-utils-build-20130529-1.el7.x86_64
/opt/rh/autotools-latest/root/lib64
%endif
%if 0%{?rhel} <= 6
%_datadir/aclocal
%endif


%changelog
* Fri Oct 12 2018 Pavel Raiskup <praiskup@redhat.com> - 1-24
- provide /usr/share/aclocal

* Fri Oct 12 2018 Pavel Raiskup <praiskup@redhat.com> - 1-23
- br: compat-rpm-config

* Thu Oct 11 2018 Pavel Raiskup <praiskup@redhat.com> - 1-22
- new rebuild, thanks to V. Ondruch we don't have to depend on
  environment-modules

* Fri Feb 17 2017 Pavel Raiskup <praiskup@redhat.com> - 1-20
- Bump for rhbz#1409277, scl-utils rebuild in copr temporarily

* Sat Dec 31 2016 Pavel Raiskup <praiskup@redhat.com> - 1-19
- Fedora 26 added to copr

* Sun Oct 09 2016 Pavel Raiskup <praiskup@redhat.com> - 1-18
- don't depend on epel-rpm-macros, scl macros are not working with
  epel-rpm-macros unfortunately, provide build package

* Thu Sep 29 2016 Pavel Raiskup <praiskup@redhat.com> - 1-16
- BuildRoot is broken with epel-rpm-macros (rhbz#1379684)

* Tue Sep 27 2016 Pavel Raiskup <praiskup@redhat.com> - 1-15
- bump: rebuild september 2016
- remove some compat stuff now implemented in epel-rpm-macros

* Wed Aug 12 2015 Pavel Raiskup <praiskup@redhat.com> - 1-14
- Requires field must be spearated by spaces and not commas

* Wed Aug 12 2015 Pavel Raiskup <praiskup@redhat.com> - 1-13
- use _compat_el5_build only if defined (rhbz#1252751)

* Tue Jun 23 2015 Pavel Raiskup <praiskup@redhat.com> - 1-12
- make the meta-packages architecture dependant

* Thu Jun 11 2015 Pavel Raiskup <praiskup@redhat.com> - 1-11
- fix for scl-utils 2.0 (environment modules)

* Fri Aug 15 2014 Pavel Raiskup <praiskup@redhat.com> - 1-10
- rebuilt

* Sat Jul 19 2014 Pavel Raiskup <praiskup@redhat.com> - 1-9
- merge changes from autotools-git.spec

* Thu May 29 2014 Pavel Raiskup <praiskup@redhat.com> - 1-8
- release bump for %%_compat_el5_build

* Fri Apr 18 2014 Pavel Raiskup <praiskup@redhat.com> - 1-7
- the fix for 'filelist' (#1079203) is not needed, according to
  https://fedorahosted.org/SoftwareCollections/ticket/18

* Thu Apr 17 2014 Pavel Raiskup <praiskup@redhat.com> - 1-6
- merge fixes with autotools-git version

* Tue Mar 25 2014 Pavel Raiskup <praiskup@redhat.com> - 1-5
- buildroots are prepared, lets require all packages

* Tue Mar 25 2014 Pavel Raiskup <praiskup@redhat.com> - 1-4
- fixes for RHEL5

* Fri Mar 21 2014 Pavel Raiskup <praiskup@redhat.com> - 1-3
- ok, this is annoying but I overlooked the mistake

* Fri Mar 21 2014 Pavel Raiskup <praiskup@redhat.com> - 1-2
- oh well, the EPEL7 workaround causes problem now on EPEL7, giving up and not
  trying to observe what really happens

* Fri Mar 21 2014 Pavel Raiskup <praiskup@redhat.com> - 1-1
- initial packaging
