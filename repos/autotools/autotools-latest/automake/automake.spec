%{?_compat_el5_build}

# Workaround to always have %%scl defined (for testing locally)
%{!?scl:%global scl autotools-latest}

%{?scl:%scl_package automake}

%global api_version 1.16

# run "make check" by default
%bcond_without check
# Run optional test
%bcond_without automake_enables_optional_test

# remove once %%configure is used instead of ./configure
%{!?_pkgdocdir: %global _pkgdocdir %{_docdir}/%{name}-%{version}}

Summary:    A GNU tool for automatically creating Makefiles
Name:       %{?scl_prefix}automake
Version:    %{api_version}.1
Release:    4%{?dist}

# docs ~> GFDL, sources ~> GPLv2+, mkinstalldirs ~> PD and install-sh ~> MIT
License:    GPLv2+ and GFDL and Public Domain and MIT

Group:      Development/Tools
Source:     ftp://ftp.gnu.org/gnu/automake/automake-%{version}.tar.gz
Source2:    http://git.savannah.gnu.org/cgit/config.git/plain/config.sub
Source3:    http://git.savannah.gnu.org/cgit/config.git/plain/config.guess

# Keep those patches in 'git format-patch' format (with docs).
%if %{with check} && !%{without automake_enables_optional_test}
Patch0:     automake-1.15-disable-vala-tests.patch
%endif

URL:        http://www.gnu.org/software/automake/
Requires:   %{?scl_prefix}autoconf >= 2.65

# requirements not detected automatically (#919810)
Requires:   perl(Thread::Queue)
Requires:   perl(threads)

BuildRequires: %{scl_prefix}autoconf
BuildRequires:  coreutils
BuildRequires:  findutils
BuildRequires:  help2man
BuildRequires:  make
# Filtering macros are in perl-macros (fedora) and (perl-devel rhel6), not in
# RHEL5
%if 0%{?rhel} == 6
BuildRequires: perl-devel
%endif
%if 0%{?rhel} == 7
BuildRequires: perl-macros
%endif
%if 0%{?fedora} || 0%{?rhel} > 7
BuildRequires:  perl-generators
%endif
BuildRequires:  perl-interpreter
BuildRequires:  perl(Thread::Queue)
BuildRequires:  perl(threads)

Requires(post): /sbin/install-info
Requires(preun): /sbin/install-info

BuildArch:  noarch

%{?scl:
BuildRequires:  scl-utils-build
Requires:       %scl_runtime
}


# for better tests coverage:
%if %{with check}
%if %{with automake_enables_optional_test}
BuildRequires: %{?scl_prefix}automake
BuildRequires: bison
BuildRequires: cscope
BuildRequires: dejagnu
%if 0%{?rhel} != 6
BuildRequires: emacs
%endif
BuildRequires: expect
BuildRequires: flex
BuildRequires: gcc-gfortran
BuildRequires: gettext-devel
BuildRequires: java-devel-openjdk
# BuildRequires: libtool # TODO: add from SCL
BuildRequires: ncompress
BuildRequires: sharutils
%if 0%{?rhel} != 5
BuildRequires: texlive-dvips
%endif
BuildRequires: texinfo-tex
BuildRequires: vala
%if !0%{?rhel:1}
BuildRequires: gcc-objc
BuildRequires: gcc-objc++
BuildRequires: imake
BuildRequires: lzip
%endif
%endif
%endif

%if ! 0%{?rhel} == 5
# remove bogus Automake perl dependencies and provides
%global __requires_exclude %{?__requires_exclude:%__requires_exclude|}^perl\\(Automake::
%global __provides_exclude %{?__provides_exclude:%__provides_exclude|}^perl\\(Automake::
%endif

%description
Automake is a tool for automatically generating `Makefile.in'
files compliant with the GNU Coding Standards.

You should install Automake if you are developing software and would
like to use its ability to automatically generate GNU standard
Makefiles.

%prep
%setup -q -n automake-%{version}
%{?scl_enable}
%if %{with check} && !%{without automake_enables_optional_test}
%patch0 -p1 -b .disable_tests
autoreconf -iv
%endif

for file in %SOURCE2 %SOURCE3; do
    for dest in $(find -name "$(basename "$file")"); do
        cp "$file" "$dest"
    done
done
%{?scl_disable}


%build
%if 0%{?fedora}
%{?scl_enable}
# disable replacing config.guess and config.sub from redhat-rpm-config
%global _configure_gnuconfig_hack 0
%configure --docdir=%{_pkgdocdir}
%make_build
%{?scl_disable}
%else
# Use ./configure where rhbz#991613 is not fixed
%{?scl_enable}
./configure --prefix=%{_prefix} --mandir=%{_mandir} --infodir=%{_infodir} \
   --bindir=%{_bindir} --datadir=%{_datadir} --libdir=%{_libdir} \
   --docdir=%{_pkgdocdir} --disable-silent-rules
%make_build V=0
%{?scl_disable}
%endif

cp m4/acdir/README README.aclocal
cp contrib/multilib/README README.multilib

%install
%_compat_install

%{?scl_enable}
%make_install
%{?scl_disable}

# %%doc is broken on older RHEL systems so that even if we set --docdir to
# %%_pkgdocdir by configure, procesing of %%doc removes whole %%_pkgdocdir and
# then it is filled by %%doc (relative) files.  This causes that amhello gets
# removed.
mv %{buildroot}/%{_pkgdocdir}/amhello* .
# Fail if non-empty!
rmdir %{buildroot}/%{_pkgdocdir}

%check
# %%global TESTS_FLAGS t/preproc-errmsg t/preproc-basics
%if %{with check}
%{?scl_enable}
make -k %{?_smp_mflags} check %{?TESTS_FLAGS: TESTS="%{TESTS_FLAGS}"} \
    || ( cat ./test-suite.log && false )
%{?scl_disable}
%endif

%post
/sbin/install-info %{_infodir}/automake.info.gz %{_infodir}/dir || :

%preun
if [ $1 = 0 ]; then
    /sbin/install-info --delete %{_infodir}/automake.info.gz %{_infodir}/dir || :
fi

%files
%doc AUTHORS README THANKS NEWS README.aclocal README.multilib COPYING*
%doc amhello-1.0.tar.gz
%exclude %{_infodir}/dir
%exclude %{_datadir}/aclocal
%{_bindir}/*
%{_infodir}/*.info*
%{_datadir}/automake-%{api_version}
%{_datadir}/aclocal-%{api_version}
%{_mandir}/man1/*


%changelog
* Fri Oct 12 2018 Pavel Raiskup <praiskup@redhat.com> - 1.16.1-4
- don't run emacs/lisp tests on el6 for now

* Fri Oct 12 2018 Pavel Raiskup <praiskup@redhat.com> - 1.16.1-3
- re-enable testsuite

* Fri Oct 12 2018 Pavel Raiskup <praiskup@redhat.com> - 1.16.1-2
- fix testsuite for epel, disable testsuite for bootstrap

* Fri Oct 12 2018 Pavel Raiskup <praiskup@redhat.com> - 1.16.1-1
- sync with rawhide

* Mon Oct 10 2016 Pavel Raiskup <praiskup@redhat.com> - 1.15-7
- BR perl-generators

* Mon Oct 10 2016 Pavel Raiskup <praiskup@redhat.com> - 1.15-6
- bump: rebuild october 2016

* Fri Jun 17 2016 Pavel Raiskup <praiskup@redhat.com> - 1.15-5
- bump

* Wed Aug 12 2015 Pavel Raiskup <praiskup@redhat.com> - 1.15-4
- apply the "curly bracket" patch

* Wed Aug 12 2015 Pavel Raiskup <praiskup@redhat.com> - 1.15-3
- sync with Fedora 24 (rawhide) spec file

* Wed Aug 12 2015 Pavel Raiskup <praiskup@redhat.com> - 1.15-2
- use _compat_el5_build only if defined (rhbz#1252751)

* Tue Jun 23 2015 Pavel Raiskup <praiskup@redhat.com> - 1.15-1
- rebase to latest upstream release

* Tue Jun 09 2015 Pavel Raiskup <praiskup@redhat.com> - 1.14.1-6
- BR perl-Thread-Queue to fix help2man run

* Thu May 29 2014 Pavel Raiskup <praiskup@redhat.com> - 1.14.1-4
- release bump for %%_compat_el5_build

* Tue Mar 25 2014 Pavel Raiskup <praiskup@redhat.com> - 1.14.1-3
- merge fixes for RHEL5 from autotools-git

* Fri Mar 21 2014 Pavel Raiskup <praiskup@redhat.com> - 1.14.1-2
- c&p mistake

* Fri Feb 28 2014 Pavel Raiskup <praiskup@redhat.com> - 1.14.1-1
- copy SCL-ized spec file from autotools-git
