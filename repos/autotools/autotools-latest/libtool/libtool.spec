%{?_compat_el5_build}
%{!?scl:%global scl autotools-latest}
%{?sclize_package}

%{?scl:%global __provides_exclude ^libltdl\\.so.*$}

%if 0%{?scl:1}
%global gcc_major  %(scl enable %scl 'gcc -dumpversion' || echo 666)
%global automake_version %(scl enable %scl 'set -- `automake --version` ; echo ${4-unknown}' || echo 666)
%else
# See the bug #429880
%global gcc_major  %(gcc -dumpversion || echo "666")
# See rhbz#1193591
%global automake_version %(set -- `automake --version | head -n 1` ; echo ${4-unknown})
%endif

%{?scl:%scl_package libtool}

%bcond_without check

Summary: The GNU Portable Library Tool
Name:    %{?scl_prefix}libtool
Version: 2.4.6
Release: 9%{?dist}
License: GPLv2+ and LGPLv2+ and GFDL
URL:     http://www.gnu.org/software/libtool/
Group:   Development/Tools

Source:  http://ftp.gnu.org/gnu/libtool/libtool-%{version}.tar.gz

# ~> downstream
# ~> remove possibly once #1158915 gets fixed somehow
Patch0:  libtool-2.4.5-rpath.patch

# ~> downstream (TODO)
Patch1: libtool-2.4.6-am-1.16-test.patch

# ~> upstream 702a97fbb
Patch2: libtool-2.4.6-specs.patch

# See the rhbz#1289759 and rhbz#1214506.  We disable hardening namely because
# that bakes the CFLAGS/LDFLAGS into installed /bin/libtool and ltmain.sh files.
# At the same time we want to have libltdl.so hardened.  Downstream-only patch.
%undefine _hardened_build
Patch3: libtool-2.4.6-hardening.patch

# rhbz#1622611, upstream 350082b6aa89f9ef603fcebbb4cf33f15a743f2f
Patch4: libtool-2.4.6-fatal-handler.patch

%if ! 0%{?_module_build}
Patch100: libtool-nodocs.patch
%endif

# /usr/bin/libtool includes paths within gcc's versioned directories
# Libtool must be rebuilt whenever a new upstream gcc is built
# Starting with gcc 7 gcc in Fedora is packaged so that only major
# number changes need libtool rebuilding.
%if 0%(echo %gcc_major | sed 's|\..*||g') > 6
Requires: gcc(major) = %{gcc_major}
%else
Requires: gcc = %{gcc_major}
%endif
Requires: %{scl_prefix}autoconf, %{scl_prefix}automake, sed, tar, findutils
Requires(post):  /sbin/install-info
Requires(preun): /sbin/install-info

%if ! 0%{?_module_build}
BuildRequires: texinfo
%endif
BuildRequires: %{scl_prefix}autoconf, %{scl_prefix}automake

%if 0%{?scl:1}
BuildRequires: scl-utils-build %{scl_prefix}runtime %{scl_prefix}m4
Requires:%scl_runtime
%endif
BuildRequires: help2man

# make sure we can configure all supported langs
BuildRequires: libstdc++-devel, gcc-gfortran

BuildRequires: gcc, gcc-c++


%description
GNU Libtool is a set of shell scripts which automatically configure UNIX and
UNIX-like systems to generically build shared libraries. Libtool provides a
consistent, portable interface which simplifies the process of using shared
libraries.

If you are developing programs which will use shared libraries, but do not use
the rest of the GNU Autotools (such as GNU Autoconf and GNU Automake), you
should install the libtool package.

The libtool package also includes all files needed to integrate the GNU
Portable Library Tool (libtool) and the GNU Libtool Dynamic Module Loader
(ltdl) into a package built using the GNU Autotools (including GNU Autoconf
and GNU Automake).


%package ltdl
Summary:  Runtime libraries for GNU Libtool Dynamic Module Loader
Group:    System Environment/Libraries
Provides: %{?scl_prefix}%{name}-libs%{_isa} = %{version}-%{release}
License:  LGPLv2+
Requires(post):  /sbin/ldconfig
Requires(postun):  /sbin/ldconfig
%{?scl:Requires:%scl_runtime}

%description ltdl
The libtool-ltdl package contains the GNU Libtool Dynamic Module Loader, a
library that provides a consistent, portable interface which simplifies the
process of using dynamic modules.

These runtime libraries are needed by programs that link directly to the
system-installed ltdl libraries; they are not needed by software built using
the rest of the GNU Autotools (including GNU Autoconf and GNU Automake).


%package ltdl-devel
Summary: Tools needed for development using the GNU Libtool Dynamic Module Loader
Group:    Development/Libraries
Requires: %{?scl_prefix}automake = %automake_version
Requires: %{name}-ltdl = %{version}-%{release}
License:  LGPLv2+
%{?scl:Requires:%scl_runtime}

%description ltdl-devel
Static libraries and header files for development with ltdl.


%prep
%setup -q -n libtool-%version

%patch0 -p1 -b .rpath
%patch1 -p1 -b .test
%patch2 -p1 -b .gcc-specs
%patch3 -p1 -b .ltdl-hardening
%patch4 -p1 -b .fatal-handler
%if ! 0%{?_module_build}
%patch100 -p1 -b .nodocs
%endif

%{?scl_enable}
autoreconf -v
%{?scl_disable}

%build
%{?scl_enable}
export CC=gcc
export CXX=g++
export F77=gfortran
export CFLAGS="$RPM_OPT_FLAGS -fPIC"

%configure  --prefix=%{_prefix}                 \
            --exec-prefix=%{_prefix}            \
            --bindir=%{_bindir}                 \
            --sbindir=%{_sbindir}               \
            --sysconfdir=%{_sysconfdir}         \
            --datadir=%{_datadir}               \
            --includedir=%{_includedir}         \
            --libdir=%{_libdir}                 \
            --libexecdir=%{_libexecdir}         \
            --localstatedir=%{_localstatedir}   \
            --mandir=%{_mandir}                 \
            --infodir=%{_infodir}

%make_build \
    CUSTOM_LTDL_CFLAGS="%{?_hardening_cflags}" \
    CUSTOM_LTDL_LDFLAGS="%{?_hardening_ldflags}"
%{?scl_disable}


%check
%if %{with check}
%{?scl_enable}
make check VERBOSE=yes || { cat testsuite.log ; false ; }
%{?scl_disable}
%endif


%install
%{?scl_enable}
%_compat_install
make install DESTDIR=%{buildroot}
# info's TOP dir (by default owned by info)
rm -f %{buildroot}%{_infodir}/dir
# *.la *.a files generated by libtool shouldn't be distributed (and the
# `./configure --disable-static' breaks testsuite)
rm -f %{buildroot}%{_libdir}/libltdl.{a,la}
%{?scl_disable}

%post
/sbin/install-info %{_infodir}/libtool.info.gz %{_infodir}/dir || :


%post ltdl -p /sbin/ldconfig


%preun
if [ "$1" = 0 ]; then
   /sbin/install-info --delete %{_infodir}/libtool.info.gz %{_infodir}/dir || :
fi


%postun ltdl -p /sbin/ldconfig


%files
%doc AUTHORS COPYING NEWS README THANKS TODO ChangeLog*
%{_infodir}/libtool.info*.gz
%{_mandir}/man1/libtool.1*
%{_mandir}/man1/libtoolize.1*
%{_bindir}/libtool
%{_bindir}/libtoolize
%{_datadir}/aclocal/*.m4
%dir %{_datadir}/libtool
%{_datadir}/libtool/build-aux


%files ltdl
%doc libltdl/COPYING.LIB
%{_libdir}/libltdl.so.*


%files ltdl-devel
%doc libltdl/README
%{_datadir}/libtool
%exclude %{_datadir}/libtool/build-aux
%{_includedir}/ltdl.h
%{_includedir}/libltdl
# .so files without version must be in -devel subpackage
%{_libdir}/libltdl.so


%changelog
* Fri Oct 12 2018 Pavel Raiskup <praiskup@redhat.com> - 2.4.6-9
- fix build for epel6, fix automake requires

* Fri Oct 12 2018 Pavel Raiskup <praiskup@redhat.com> - 2.4.6-8
- sync with fedora

* Tue Apr 18 2017 Pavel Raiskup <praiskup@redhat.com> - 2.4.6-7
- revert gcc_major fix for Fedora <= 25

* Fri Feb 17 2017 Pavel Raiskup <praiskup@redhat.com> - 2.4.6-5
- bump for gcc 7 in F26

* Fri Feb 17 2017 Pavel Raiskup <praiskup@redhat.com> - 2.4.6-5
- bump for gcc 7 in F26

* Sat Dec 31 2016 Pavel Raiskup <praiskup@redhat.com> - 2.4.6-4
- bump: gcc 6.3.X

* Mon Oct 10 2016 Pavel Raiskup <praiskup@redhat.com> - 2.4.6-3
- bump: rebuild october 2016

* Mon Oct 10 2016 Pavel Raiskup <praiskup@redhat.com> - 2.4.6-2
- bump: rebuild october 2016

* Fri Jun 17 2016 Pavel Raiskup <praiskup@redhat.com> - 2.4.6-12
- rebase and rebuild for new gcc, sync with fedora rawhide

* Tue Jun 14 2016 Pavel Raiskup <praiskup@redhat.com> - 2.4.2-17
- rebuild for new gcc versions

* Wed Dec 16 2015 Pavel Raiskup <praiskup@redhat.com> - 2.4.2-16
- rebuild for new gcc 5.3.1 in f22

* Mon Dec 14 2015 Pavel Raiskup <praiskup@redhat.com> - 2.4.2-15
- filter provides only if %%scl is defined

* Mon Dec 14 2015 Pavel Raiskup <praiskup@redhat.com> - 2.4.2-14
- do not provide libltdl.so, that breaks initial buildroot

* Sun Dec 13 2015 Pavel Raiskup <praiskup@redhat.com> - 2.4.2-13
- make sure collection is enabled during %%install

* Sun Dec 13 2015 Pavel Raiskup <praiskup@redhat.com> - 2.4.2-11
- bump for new gcc in f22+

* Wed Aug 12 2015 Pavel Raiskup <praiskup@redhat.com> - 2.4.2-10
- help2man BR

* Wed Aug 12 2015 Pavel Raiskup <praiskup@redhat.com> - 2.4.2-9
- use _compat_el5_build only if defined (rhbz#1252751)

* Sat Feb 07 2015 Pavel Raiskup <praiskup@redhat.com> - 2.4.2-8
- rebuild for new gcc rawhide

* Fri Dec 19 2014 Pavel Raiskup <praiskup@redhat.com> - 2.4.2-7
- rebuild for the new gcc

* Sun Jul 13 2014 Pavel Raiskup <praiskup@redhat.com> - 2.4.2-6
- rebuild for new gcc in F19, second try

* Fri Jul 11 2014 Pavel Raiskup <praiskup@redhat.com> - 2.4.2-5
- rebuild for new gcc in F19

* Sat Jun 28 2014 Pavel Raiskup <praiskup@redhat.com> - 2.4.2-4
- rebuild for new gcc 4.8.3
- gcc-java removed from Fedora completely (#1106080)

* Thu May 29 2014 Pavel Raiskup <praiskup@redhat.com> - 2.4.2-3
- release bump for %%_compat_el5_build

* Tue Mar 25 2014 Pavel Raiskup <praiskup@redhat.com> - 2.4.2-2
- RHEL5 fixes

* Fri Mar 21 2014 Pavel Raiskup <praiskup@redhat.com> - 2.4.2-1
- copy SCL-ized spec from autotools-git
